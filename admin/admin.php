<?php include '../layouts/PDO.php';?>
<!doctype html>

<html>
<head>
<!-- tous nos trucs du head ici -->
<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
    <link rel="stylesheet" href="/css/back.css">

</head>

<body>
<div class="connexion-container">

<div class="form-container">

    <form action="/admin/produits/gestprod.php" method="post" class="form">

    <div class="form-group">
        <label for="connexion" class="custom-label">adresse mail :</label>
        <input type="email" class="custom-input-text" id="email" name="email" required>
    </div>

    <div class="form-group">
        <label for="password" class="custom-label">Mot de passe :</label>
        <input type="password" class="custom-input-text" id="mdp" name="mdp" required>
    </div>


     <div class="submit-container">
        <button class="btn btn-custom-blue submit-button" type="submit">Me connecter</button>
    </div>
    </form>
</div>

</div>
</body>
</html>
