<?php require('../../layouts/back-head.php'); ?>


<?php
	if(isset($_SESSION['connecte']) && $_SESSION['connecte'] == true)
	{
		?>
		<div class="form-container">

			<h1>Créer un produit</h1>

			<?php
				// DETERMINER LE DERNIER ID DISPONIBLE //
				$id = 0;
				// PREPARATION REQUETE //
				$requete = $bdd->prepare(''
							. 'SELECT ref_produit '
							. 'FROM produit'
							. '');
				// LANCEMENT REQUÊTE //
				$requete->execute();
				// STOCKAGE DU RESULTAT DE LA REQUÊTE //
				$lesIdProduits = $requete->fetchAll();
				// Trouver l'id de la base le plus grand //
				foreach($lesIdProduits as $idProduit)
				{
					if($idProduit['ref_produit'] >= $id)
					{
						$id = $idProduit['ref_produit'];
					}
					// Rajouter 1 //
					$id = $id + 1;
				}
		 	?>

			<!-- Le formulaire renvoie vers sa propore page -->
			<form action="./gestprod.php" method="post" class="form">

				<!-- Détermination automatique de l'ID -->
				<input type="text" class="custom-input-text" id="ref_produit" name="ref_produit" value="<?php echo $id ?>" hidden>

				<div class="form-group">
					<label for="productName" class="custom-label">Nom du produit :</label>
					<input type="text" class="custom-input-text" id="nom_produit" name="nom_produit">
				</div>

				<div class="form-group">
					<label for="productDescription" class="custom-label">Description du produit :</label>
					<textarea class="custom-textarea" id="description" name="description"></textarea>
				</div>

				<!-- TODO : upload de fichier via PHP
				<div class="form-group">
					<label for="productPicture" class="custom-label">Photo du produit :</label>
					<input type="file" class="custom-input-file">
				</div>
				-->
				<div class="form-group">
					<label for="productPicture" class="custom-label">Photo du produit :</label>
					<input type="text" class="custom-input-text" id="image" name="image">
				</div>

				<?php
					// PREPARATION REQUETE POUR LES CATEGORIES //
				    $requete = $bdd->prepare(''
				                . 'SELECT * '
				                . 'FROM categorie'
				                . '');
				    // LANCEMENT REQUÊTE //
				    $requete->execute();
				    // STOCKAGE DU RESULTAT DE LA REQUÊTE //
				    $lesCategories = $requete->fetchAll();
				?>

				<div class="form-group">
					<label for="productCategory" class="custom-label">Catégorie du produit :</label>
					<select class="custom-dropdown" id="id_categorie" name="id_categorie">
						<?php
						foreach($lesCategories as $categorie)
						{
							?>
							<option value="<?php echo $categorie['id_categorie'] ?>"> <?php echo $categorie['nom_categorie'] ?></option>
							<?php
						}
						?>
					</select>
				</div>

				<div class="submit-container">
					<button class="btn btn-custom-blue submit-button" type="submit">Créer</button>
				</div>
			</form>
		</div>
		<?php
	}
	// SI NON ADMIN //
	else
	{
		echo "Vous n'êtes pas administrateur";
	}
?>
