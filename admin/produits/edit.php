<?php require('../../layouts/back-head.php'); ?>

<?php
	if(isset($_SESSION['connecte']) && $_SESSION['connecte'] == true)
	{
		// RECEPTION D'UN UPDATE DE PRODUIT //
		if (isset($_GET['ref_prod_modif']))
		{
			// PREPARATION REQUETE POUR LES PRODUIT //
			$requete = $bdd->prepare(''
						. 'SELECT * '
						. 'FROM produit '
						. '');
			// LANCEMENT REQUÊTE //
			$requete->execute();
			// STOCKAGE DU RESULTAT DE LA REQUÊTE //
			$lesProduits = $requete->fetchAll();

			foreach($lesProduits as $produit)
			{
				if($produit['ref_produit'] == $_GET['ref_prod_modif'])
				{
					?>
					<div class="form-container">

						<h1>Modifier un produit</h1>

						<form action="gestprod.php" method="post" class="form">

							<!-- Détermination automatique de l'ID -->
							<input type="text" class="custom-input-text" id="ref_prod_modif" name="ref_prod_modif" value="<?php echo $_GET['ref_prod_modif'] ?>" hidden>

							<div class="form-group">
								<label for="productName" class="custom-label">Nom du produit :</label>
								<input type="text" class="custom-input-text" id="nom_produit" name="nom_produit" value="<?php echo $produit['nom_produit'] ?>">
							</div>

							<div class="form-group">
								<label for="productDescription" class="custom-label">Description du produit :</label>
								<textarea class="custom-textarea" id="description" name="description"><?php echo $produit['description'] ?>"</textarea>
							</div>

							<!-- TODO : upload de fichier via PHP
							<div class="form-group">
								<label for="productPicture" class="custom-label">Photo du produit :</label>
								<input type="file" class="custom-input-file">
							</div>
							-->
							<div class="form-group">
								<label for="productPicture" class="custom-label">Photo du produit :</label>
								<input type="text" class="custom-input-text" id="image" name="image" value="<?php echo $produit['image'] ?>">
							</div>

							<?php
								// PREPARATION REQUETE POUR LES CATEGORIES //
								$requete = $bdd->prepare(''
											. 'SELECT * '
											. 'FROM categorie'
											. '');
								// LANCEMENT REQUÊTE //
								$requete->execute();
								// STOCKAGE DU RESULTAT DE LA REQUÊTE //
								$lesCategories = $requete->fetchAll();
							?>

							<div class="form-group">
								<label for="productCategory" class="custom-label">Catégorie du produit :</label>
								<select class="custom-dropdown" id="id_categorie" name="id_categorie">
									<?php
									foreach($lesCategories as $categorie)
									{
										?>
										<option value="<?php echo $categorie['id_categorie'] ?>" <?php if($categorie['id_categorie'] == $produit['id_categorie']){echo "selected";} ?> > <?php echo $categorie['nom_categorie'] ?></option>
										<?php
									}
									?>
								</select>
							</div>
							<div class="submit-container">
								<button class="btn btn-custom-blue submit-button" type="submit">Modifier</button>
							</div>
						</form>
					</div>
					<?php

				}
				// ARRIVEE SUR LA PAGE AVEC UN REF_PRODUIT DE MERDE EN GET //
				else
				{
					?>
					<?php
				}
			}
		}
		// ARRIVEE SUR LA PAGE SANS REF_PRODUIT EN GET //
		else
		{
			?>
			<?php
		}
	}
	// SI NON ADMIN //
	else
	{
		echo "Vous n'êtes pas administrateur";
	}


?>
