<?php require('../../layouts/back-head.php'); ?>

<?php
	if(isset($_SESSION['connecte']) && $_SESSION['connecte'] == true)
	{

		// RECEPTION D'UN EVENTUEL INSERT DE PRODUIT //
		if (isset($_POST['ref_produit']) && isset($_POST['id_categorie']) && isset($_POST['nom_produit']) && isset($_POST['description']) && isset($_POST['image']))
		{
			// PREPARER L'INSERT //
			$requete = $bdd->prepare(''
					. 'INSERT INTO produit '
					. '('
					. 'ref_produit' . ','
					. 'id_categorie' . ','
					. 'nom_produit' . ','
					. 'description' . ','
					. 'image'
					. ') '
					. 'VALUES(?, ?, ?, ?, ?);');
			// LANCEMENT REQUÊTE //
			$requete->execute(array(
				$_POST['ref_produit'],
				$_POST['id_categorie'],
				$_POST['nom_produit'],
				$_POST['description'],
				$_POST['image'],
			));
		}
	?>

	<?php
		// RECEPTION D'UN EVENTUEL DELETE DE PRODUIT //
		if(isset($_GET['ref_prod_suppr']))
		{
			// PREPARATION REQUETE SUPPRESSION //
			$requete = $bdd->prepare(''
						. 'DELETE FROM produit '
						. 'WHERE   ref_produit = '.$_GET['ref_prod_suppr']
						. '');
			// LANCEMENT REQUÊTE //
			$requete->execute();
		}
	?>

	<?php
		// RECEPTION D'UN EVENTUEL UPDATE DE PRODUIT //
		if(isset($_POST['ref_prod_modif']) && isset($_POST['id_categorie']) && isset($_POST['nom_produit']) && isset($_POST['description']) && isset($_POST['image']))
		{
			// PREPARATION REQUETE UPDATE //
			$requete = $bdd->prepare(''
				. 'UPDATE produit '
				. 'SET id_categorie = ?, '
				. 'nom_produit = ?, '
				. 'description = ?, '
				. 'image = ? '
				. 'WHERE ref_produit = ?'
				. ';');
			// LANCEMENT REQUÊTE //
			$requete->execute(array(
				$_POST['id_categorie'],
				$_POST['nom_produit'],
				$_POST['description'],
				$_POST['image'],
				$_POST['ref_prod_modif']
			));
		}
	?>

	<div class="products-container">
	    <h1>Gérer les produits</h1>

	    <div class="products-controls">
	        <a class="btn btn-custom-blue"
	                href="/admin/produits/create.php"
	                style="text-decoration: none">
	                <i class="fas fa-plus"></i> Créer un nouveau produit
	        </a>
	    </div>

	    <div class="products">
	        <div class="product-header">
	            <p class="p-column">Image</p>
	            <p class="p-column">ID</p>
	            <p class="p-column">Nom</p>
	            <p class="p-column">Catégorie</p>
	            <p class="p-column">Actions</p>
	        </div>

			<?php
				// PREPARATION REQUETE POUR LES PRODUITS //
				$requete = $bdd->prepare(''
							. 'SELECT produit.*, categorie.nom_categorie '
							. 'FROM produit '
							. 'JOIN categorie '
							. 'ON produit.id_categorie = categorie.id_categorie'
							. '');
				// LANCEMENT REQUÊTE //
				$requete->execute();
				// STOCKAGE DU RESULTAT DE LA REQUÊTE //
				$lesProduits = $requete->fetchAll();
			?>

			<?php
			foreach($lesProduits as $produit)
			{
				?>
					<div class="product-content">
						<div class="product-content-image p-column">
							<img src="<?php echo $produit['image'] ?>" alt="">
						</div>

						<p class="p-column"><?php echo $produit['ref_produit'] ?></p>
						<p class="p-column"><?php echo $produit['nom_produit'] ?></p>
						<p class="p-column"><?php echo $produit['nom_categorie'] ?></p>

						<div class="product-content-actions p-column">
							<div class="product-action-button">
								<a href="/admin/produits/edit.php?ref_prod_modif=<?php echo $produit['ref_produit'] ?>"><i class="fas fa-edit"></i></a>
							</div>
							<div class="product-action-button">
								<div class="modal-container">
									<input id="modal-toggle" type="checkbox">
									<a href="#"><i class="fas fa-trash-alt"></i></a>
									<div class="modal-backdrop">
										<div class="modal-content">
											<label class="modal-close" for="modal-toggle">x</label>
											<h2>Suppression de produit</h2>
											<hr />
											<p>Voulez-vous vraiment supprimer ce produit ?</p>
											<p>Audi R8 Turbo 600 chevaux</p>
												<a href="./gestprod.php" class="modal-close button" for="modal-toggle">Annuler</a>
												<a href="./gestprod.php?ref_prod_suppr=<?php echo $produit['ref_produit'] ?>" class="modal-close delete-button" style="background: red; color: white;text-decoration: none">Supprimer</a>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>

				<?php
			}
			?>

	    </div>
	</div>

	</div> <!-- div admin-container -->

	</main>

	<?php
	}
	// SI NON ADMIN //
	else
	{
		echo "Vous n'êtes pas administrateur";
	}
?>
