-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  mer. 06 fév. 2019 à 00:44
-- Version du serveur :  5.7.24
-- Version de PHP :  7.2.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `bdd_boutique_de_luxe`
--

-- --------------------------------------------------------

--
-- Structure de la table `categorie`
--

DROP TABLE IF EXISTS `categorie`;
CREATE TABLE IF NOT EXISTS `categorie` (
  `id_categorie` int(11) NOT NULL,
  `nom_categorie` varchar(100) NOT NULL,
  PRIMARY KEY (`id_categorie`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `categorie`
--

INSERT INTO `categorie` (`id_categorie`, `nom_categorie`) VALUES
(1, 'Voitures'),
(2, 'Arts'),
(3, 'Bijoux');

-- --------------------------------------------------------

--
-- Structure de la table `login`
--

DROP TABLE IF EXISTS `login`;
CREATE TABLE IF NOT EXISTS `login` (
  `idLogin` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(50) NOT NULL,
  `mdp` varchar(50) NOT NULL,
  PRIMARY KEY (`idLogin`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `login`
--

INSERT INTO `login` (`idLogin`, `nom`, `mdp`) VALUES
(1, 'Xavier', 'Delenne');

-- --------------------------------------------------------

--
-- Structure de la table `produit`
--

DROP TABLE IF EXISTS `produit`;
CREATE TABLE IF NOT EXISTS `produit` (
  `ref_produit` int(11) NOT NULL,
  `id_categorie` int(11) NOT NULL,
  `nom_produit` varchar(100) NOT NULL,
  `description` varchar(1000) NOT NULL,
  `image` varchar(1000) NOT NULL,
  `alt` varchar(255) NOT NULL,
  PRIMARY KEY (`ref_produit`),
  KEY `id_categorie` (`id_categorie`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `produit`
--

INSERT INTO `produit` (`ref_produit`, `id_categorie`, `nom_produit`, `description`, `image`, `alt`) VALUES
(1, 1, 'Bugatti Veyron', 'La Bugatti 16.4 Veyron fut l\'unique modèle en production du constructeur automobile français Bugatti de 2005 à 2015. Supercar atteignant 431,072 km/h dans sa version super sport, c\'était alors la voiture de série la plus rapide du monde.', '/img/bugatti.jpg', 'Bugatti Veyron'),
(2, 1, 'Audi R8', 'L\'Audi R8 est une voiture de sport du constructeur allemand Audi. C\'est le premier coupé GT deux-places de la marque qui rivalise ainsi avec les marques historiques de ce segment : Porsche (la 911), Ferrari, Corvette ou Aston Martin.', '/img/audi.798530_640.jpg', 'Audi R8'),
(3, 2, 'Banksy Royal Muse', 'On ne connaît ni son visage, ni sa véritable identité et pourtant, sa renommée n’a pas d’égal dans le milieu du street art. Il effectue ses graffitis au pochoir pour être le plus rapide possible et ainsi éviter d’être pris en flagrant délit. Ses œuvres militantes mêlent ironie, humour noir et poésie. Zoom sur le travail du street-artiste Banksy.', '/img/banksy-art.jpg', 'Banksy Royal Muse'),
(4, 2, 'Banksy Steve Jobs', 'Le fils d\'un migrant de Syrie est une peinture murale réalisée en 2015 par le graffeur Banksy . La murale était située dans la jungle de Calais , surnom donné au campement situé près de Calais , en France, où les migrants vivaient alors qu\'ils tentaient d\'entrer au Royaume-Uni. L\'œuvre représente la fin d\' Apple co-fondateur et ancien PDG Steve Jobs -le fils d\'un Syrien migrant aux États-Unis-tant que migrants voyageant.', '/img/banksy-jobs.jpg', 'Banksy Steve Jobes'),
(5, 1, 'Mercedes', 'La Mercedes-Benz SLS AMG est un modèle du constructeur allemand Mercedes-Benz. Elle reprend les portes papillon de la 300 SL des années 1950. 5000 Mercedes SLS AMG ont été produites de 2009 à 2015 toutes versions et modèles confondus, dont 4000 coupés et 1000 roadsters.', 'img/auto.2179220_640.jpg', 'Mercedes'),
(6, 1, 'Audi', 'L\'Audi A3 est une automobile bicorps de catégorie « compacte » construite par Audi depuis 1996. La version actuelle date de 2013.\r\nElle utilise la plate-forme MQB du groupe Volkswagen qu\'elle partage avec la Volkswagen Golf VII, la Škoda Octavia III, la Seat Leon III. Le 26 juillet 2013, Audi annonce que le cap des 3 millions d\'A3 produites, toutes générations confondues.Elle est fabriquée à Ingolstadt en Allemagne.', 'img/car.604019_640.jpg', 'Audi'),
(7, 3, 'Bague en argent', 'Une bague est un article de joaillerie qui se porte généralement au doigt. De fait, elle a une forme d\'anneau, plus ou moins large, serti ou non de pierres et quelquefois gravé. Au même titre que les boucles d\'oreilles, les bagues peuvent être constituées de toutes sortes de matériaux, incluant métal, plastique, bois, os et verre.', '/img/bague.jpg', 'Bague an argent'),
(8, 3, 'Bague avec rubis', 'Une bague est un article de joaillerie qui se porte généralement au doigt. De fait, elle a une forme d\'anneau, plus ou moins large, serti ou non de pierres et quelquefois gravé. Au même titre que les boucles d\'oreilles, les bagues peuvent être constituées de toutes sortes de matériaux, incluant métal, plastique, bois, os et verre.', '/img/bague(2).jpg', 'Bague avec rubis');

-- --------------------------------------------------------

--
-- Structure de la table `role`
--

DROP TABLE IF EXISTS `role`;
CREATE TABLE IF NOT EXISTS `role` (
  `id_role` int(11) NOT NULL,
  `nom_role` varchar(100) NOT NULL,
  PRIMARY KEY (`id_role`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `role`
--

INSERT INTO `role` (`id_role`, `nom_role`) VALUES
(0, 'admin'),
(1, 'user');

-- --------------------------------------------------------

--
-- Structure de la table `utilisateur`
--

DROP TABLE IF EXISTS `utilisateur`;
CREATE TABLE IF NOT EXISTS `utilisateur` (
  `id_utilisateur` int(11) NOT NULL,
  `id_role` int(11) NOT NULL,
  `nom_utilisateur` varchar(100) NOT NULL,
  `prenom_utilisateur` varchar(100) NOT NULL,
  `mail_utilisateur` varchar(100) NOT NULL,
  `mdp_utilisateur` varchar(1000) NOT NULL,
  PRIMARY KEY (`id_utilisateur`),
  KEY `id_role` (`id_role`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `utilisateur`
--

INSERT INTO `utilisateur` (`id_utilisateur`, `id_role`, `nom_utilisateur`, `prenom_utilisateur`, `mail_utilisateur`, `mdp_utilisateur`) VALUES
(0, 0, 'admin', 'Yaniss', 'yaniss.benali@lmdsio.fr', '92easy');

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `produit`
--
ALTER TABLE `produit`
  ADD CONSTRAINT `produit_ibfk_1` FOREIGN KEY (`id_categorie`) REFERENCES `categorie` (`id_categorie`);

--
-- Contraintes pour la table `utilisateur`
--
ALTER TABLE `utilisateur`
  ADD CONSTRAINT `utilisateur_ibfk_1` FOREIGN KEY (`id_role`) REFERENCES `role` (`id_role`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
