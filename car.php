<html lang="en">
<!--<head>-->
<!--    <meta charset="UTF-8" />-->
<!--    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">-->
<!--    <meta name="viewport" content="width=device-width, initial-scale=1.0">-->
<!--    <title>Boutique de Luxe</title>-->
<!--    <link rel="stylesheet" type="text/css" href="../css/front.css" />-->
<!--</head>-->

<?php require ('layouts/front-head.php')?>



<body>
<div class="categorie">




    <div class="case-study-gallery">
        <img class="logo" src="img/Logo.png" alt="logo">
        <div class="case-study-right">
        <h1 class="title">Claire Dupond</h1>
        <p>Le luxe ne se débloque qu'avec une carte gold.</p>
        </div>
    </div>

        <div class="gallery">

            <h2>Les voitures</h2>

           <?php

    // On récupère tout le contenu de la table categorie voiture
    $reponse = $bdd->query('SELECT * FROM produit WHERE id_categorie = 1;');



    // On affiche chaque entrée une à une
    while ($donnees = $reponse->fetch())
    {
    ?>
    <div class="container">
                           <img class="catégorie_image" src="<?php echo $donnees['image']?>" alt="<?php echo $donnees['alt']?>"/>
                          
                          
                          <div class="produits">
                            <h1 ><?php echo $donnees['nom_produit']; ?></h1>
                        
                            <p class="desc"><?php echo substr($donnees['description'],0 ,120); ?></p>
                            
                            <div class="button">
                              <a href="product.php?id=<?php echo $donnees['ref_produit']?>"><button class="add"><span>Produit</span></button></a>
                            </div>
                          </div>
                        </div>
    <?php
    }

    $reponse->closeCursor(); // Termine le traitement de la requête

    ?>


        </div>
        </div>
        <?php require ('layouts/footer.php')?>

    </body>
</html>
