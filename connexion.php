<?php
session_start();
?>

<?php require ('layouts/front-head.php')?>

<body>

 <div class="case-study-gallery">
        <img class="logo" src="img/Logo.png" alt="logo">
        <div class="case-study-right">
        <h1 class="title">Claire Dupond</h1>
        <p>Le luxe ne se débloque qu'avec une carte gold.</p>
        </div>
    </div>


<?php
if(isset($_SESSION['username']))
{
?>

    <div id="loginModal" class="modal fade">
        <div class="modal-body">
            <div class="form">
                <a href="#" id="logout">
                    <button class="btn btn-danger deconnexion" type="button">Déconnexion
                    </button>
                </a>
            </div>
        </div>
    </div>


    <?php
    }
    else
    {
        ?>
        <div align="center">
            <button type="button" name="login" id="login" class="btn btn-success" data-toggle="modal" data-target="#loginModal" style="display:none;">Connexion</button>
        </div>

        <div id="loginModal" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <!--                <h4 class="modal-title">Cette action requiert des droits spécifiques</h4>-->
                        <!--                <button type="button" class="close" data-dismiss="modal">&times;</button>-->
                    </div>


                    <div class="modal-body">
                        <div class="form">
                            <form class="login-form">
                                <input type="text" placeholder="Identifiant" name="username" id="username" class="form-control"/>
                                <input type="password" placeholder="Mot de passe" name="password" id="password" class="form-control" />
                                <button type="button" name="login_button" id="login_button" class="btn btn-warning">login</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php
    }
    ?>
</body>
</html>






<script>
    $(document).ready(function(){
        $('#login_button').click(function(){
            var username = $('#username').val();
            var password = $('#password').val();
            if(username != '' && password != '')
            {
                $.ajax({
                    url:"action.php",
                    method:"POST",
                    data: {username:username, password:password},
                    success:function(data)
                    {
                        //alert(data);
                        if(data == 'No')
                        {
                            alert("Wrong Data");
                        }
                        else
                        {
                            $('#loginModal').hide();
                            location.reload();
                        }
                    }
                });
            }
            else
            {
                alert("Les deux champs sont obligatoires");
            }
        });
        $('#logout').click(function(){
            var action = "logout";
            $.ajax({
                url:"action.php",
                method:"POST",
                data:{action:action},
                success:function()
                {
                    location.reload();
                }
            });
        });
    });
</script>