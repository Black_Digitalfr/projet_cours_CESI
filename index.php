<html lang="en">
<!--<head>-->
<!--    <meta charset="UTF-8" />-->
<!--    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">-->
<!--    <meta name="viewport" content="width=device-width, initial-scale=1.0">-->
<!--    <title>Boutique de Luxe</title>-->
<!--    <link rel="stylesheet" type="text/css" href="css/front.css"/>-->
<!--</head>-->

<?php require ('layouts/front-head.php')?>

<?php
try
{
    // On se connecte à MySQL
    $bdd = new PDO('mysql:host=localhost;dbname=bdd_boutique_de_luxe;charset=utf8', 'root', '');
}
catch(Exception $e)
{
    // En cas d'erreur, on affiche un message et on arrête tout
        die('Erreur : '.$e->getMessage());
}

// Si tout va bien, on peut continuer

// On récupère tout le contenu de la table jeux_video
$reponse = $bdd->query('SELECT * FROM categorie');



$reponse->closeCursor(); // Termine le traitement de la requête

?>

<body>
    <div class="case-study-gallery">
        <img class="logo" src="img/Logo.png">
        <div class="case-study-right">
        <h1 class="title">Claire Dupond</h1>
        <p>Le luxe ne se débloque qu'avec une carte gold.</p>
        </div>
    </div>

<div class="study-container">
    <div class="case-study study1">
        <div class="case-study__overlay">
            <h2 class="case-study__title">Nos Bijoux</h2>
            <a class="case-study__link" href="jewel.php">Catalogue</a>
        </div>
    </div>


    <div class="case-study study2">

        <div class="case-study__overlay">
            <h2 class="case-study__title">Nos Voitures</h2>
            <a class="case-study__link" href="car.php">Catalogue</a>
        </div>
    </div>


    <div class="case-study study3">
        <div class="case-study__overlay">
            <h2 class="case-study__title">Nos Oeuvres d'Art</h2>
            <a class="case-study__link" href="art.php">Catalogue</a>
        </div>
    </div>

</div>


<?php require ('layouts/footer.php')?>

</body>



</html>
