$(function () {

    var $sidebarAdmin = $('.sidebar-admin');

// resize the height of the backoffice sidebar to make the full height of the page
    var resize = function () {
        var height = $(document).height();
        $('.admin-main-content').height(height);
    };
    $(document).resize(resize);
    $sidebarAdmin.height();
    $('.admin-content').height();

});