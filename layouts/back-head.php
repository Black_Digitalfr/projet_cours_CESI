<?php session_start(); ?>

<!-- à inclure dans tous les fichiers du backoffice -->

<!doctype html>

<html>

<head>
    <!-- tous nos trucs du head ici -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
    <link rel="stylesheet" href="/css/back.css">
</head>

<body>

	<?php
		include 'PDO.php';
	?>

<header class="admin-header">
    <div class="admin-menu-container">
        <?php require('partials/back-nav.php') ?>

       <!--  <div class="admin-logo">
            <img src="#" alt="logo">
        </div> -->

    </div>

	<?php
		if(!isset($_SESSION['connecte']))
		{
			$_SESSION['connecte'] = false;
		}
		// RECEPTION D'UNE EVENTUELLE CONNEXION //
		if(isset($_POST['email']) && isset($_POST['mdp']))
		{
			$requete = $bdd->prepare(''
			. 'SELECT utilisateur.*, role.* '
			. 'FROM utilisateur '
			. 'JOIN role '
			. 'ON utilisateur.id_role = role.id_role '
			. 'WHERE role.id_role = 0'
			. '');
			// LANCEMENT REQUÊTE //
			$requete->execute();
			// STOCKAGE DU RESULTAT DE LA REQUÊTE //
			$lesUtilisateurs = $requete->fetchAll();
			// PARCOURS DES UTILISATEURS //
			foreach($lesUtilisateurs as $utilisateur)
			{
				if($utilisateur['mail_utilisateur'] == $_POST['email'] && $utilisateur['mdp_utilisateur'] == $_POST['mdp'])
				{
					// SI L'UTILISATEUR EXISTE EN BASE //
					if($utilisateur['nom_role'] == "admin")
					{
						// SI ADMIN, AUTORISATION EN SESSION //
						$_SESSION['id_utilisateur'] = $utilisateur['id_utilisateur'];
						$_SESSION['connecte'] = true;
					}
				}
			}
		}
		// Si le paramètre "deco" existe dans le GET, peut importe sa valeur ; deconnexion //
		if(isset($_GET['deco']))
		{
			$_SESSION['connecte'] = false;
		}
	?>

    <div class="laptop-nav"><nav role="navigation">
            <ul class="laptop-menu list-group-items">
            <li class="list-item"><a href="/admin/produits/gestprod.php" class="admin-links">Gérer les produits</a></li>
            <li class="list-item"><a href="/admin/produits/create.php" class="admin-links">Créer un produit</a></li>
            <li class="list-item"><a href="/" class="admin-links">Retourner sur le site</a></li>
			<?php
				if(isset($_SESSION['connecte']) && $_SESSION['connecte'] == true)
				{
					?>
						<li class="list-item"><a href="/admin/admin.php?deco=true" class="admin-links">Deconnexion</a></li>
					<?php
				}
				else
				{
					?>
						<li class="list-item"><a href="/admin/admin.php" class="admin-links">Connexion</a></li>
					<?php
				}
			?>
            </ul>
        </nav></div>

</header>

<main role="main" class="admin-main">

    <div class="admin-container">
<!-- fin du fichier layout -->
