<!-- inclure ici tous les éventuels scripts -->

</main>


    <footer class="flex-rw">

        <ul class="footer-list-top">
            <li>
                <h4 class="footer-list-header">Boutique de luxe</h4></li>
            <li><a href='#' class="generic-anchor footer-list-anchor">Nos bijoux</a></li>
            <li><a href='#' class="generic-anchor footer-list-anchor">Nos voitures</a></li>
            <li><a href='#' class="generic-anchor footer-list-anchor">Nos oeuvres d'art</a></li>
        </ul>
        
        <ul class="footer-list-top">
            <li id='help'>
                <h4 class="footer-list-header">Nous contacter</h4></li>
            <li><a href='mailto:contact@mail.com' class="generic-anchor footer-list-anchor">Contact</a></li>
        </ul>

      <section class="footer-social-section flex-rw">
      <span class="footer-social-overlap footer-social-connect">
      Nos <span class="footer-social-small">réseaux</span>
      </span>
            <span class="footer-social-overlap footer-social-icons-wrapper">
      <a href="https://www.facebook.com/" class="generic-anchor" target="_blank" title="Facebook" itemprop="significantLink"><i class="fab fa-facebook-f"></i></a>
      <a href="https://twitter.com/" class="generic-anchor" target="_blank" title="Twitter" itemprop="significantLink"><i class="fab fa-twitter"></i></a>
      <a href="http://instagram.com/" class="generic-anchor" target="_blank" title="Instagram" itemprop="significantLink"><i class="fab fa-instagram"></i></a>
      <a href="https://www.youtube.com/" class="generic-anchor" target="_blank" title="Youtube" itemprop="significantLink"><i class="fab fa-youtube"></i></a>
      </span>
        </section>
        <section class="footer-bottom-section flex-rw">
            <div class="footer-bottom-wrapper">
                <i class="fa fa-copyright" role="copyright">

                </i> 2019 Boutique de luxe in <address class="footer-address" role="company address">Ecully</address><span class="footer-bottom-rights"> - Tous droits réservés - </span>
            </div>
<!--            <div class="footer-bottom-wrapper">-->
<!--                <a href="/terms-of-use.html" class="generic-anchor" rel="nofollow">Terms</a> | <a href="/privacy-policy.html" class="generic-anchor" rel="nofollow">Privacy</a>-->
<!--            </div>-->
        </section>
    </footer>

</footer>
</body>

</html>

<!-- à inclure en bas de tous les fichiers