<!-- à inclure dans tous les fichiers du frontoffice -->


<!doctype html>

<html>

<head>

    <!-- tous nos trucs du head ici -->
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="Boutique de luxe - Claire Dupond" content="Claire Dupond, professionnelle dans la mise en relation et le dénichage d'excellents produits de luxe, vous conseille et vous accompagne dans votre recherche pour investir dans le bien de vos rêves." />
    <title>Claire Dupond - Boutique de Luxe</title>
    <link rel="stylesheet" type="text/css" href="../css/front.css"/>
    <link href="https://fonts.googleapis.com/css?family=Oswald" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
    <script src="../jquery.js"></script>

    <?php
        try
        {
            // On se connecte à MySQL
            $bdd = new PDO('mysql:host=localhost;dbname=bdd_boutique_de_luxe;charset=utf8', 'root', '');

        }
        catch(Exception $e)
        {
            // En cas d'erreur, on affiche un message et on arrête tout
            die('Erreur : '.$e->getMessage());
        }

    ?>


</head>

<body>
<header>
    <?php require('partials/front-nav.php') ?>
</header>



<!-- fin du fichier layout -->