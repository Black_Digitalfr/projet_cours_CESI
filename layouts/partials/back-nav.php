<nav role="navigation">
    <div id="menuToggle">
        <!--
        un faux input pour déclencher le :checked au clic
        -->
        <input type="checkbox" />

        <!--
        des spans pour simuler le menu hamburger
        -->
        <span></span>
        <span></span>
        <span></span>

        <!--
        le fameux menu
        -->
        <ul id="mobileMenu" class="list-group-items">
            <li class="list-item"><a href="/admin/dashboard.php" class="admin-links">Tableau de bord</a></li>
            <li class="list-item"><a href="/admin/produits/gestprod.php" class="admin-links">Gérer les produits</a></li>
            <li class="list-item"><a href="/admin/produits/create.php" class="admin-links">Créer un produit</a></li>
            <li class="list-item"><a href="/" class="admin-links">Retourner sur le site</a></li>

        </ul>
    </div>


</nav>