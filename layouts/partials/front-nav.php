<div class="menu-classique"
<nav role="navigation">
    <div class="menuToggle">
        <ul id="menu-header">
            <a href="../../index.php"><li>Accueil</li></a>
            <a href="../../jewel.php"><li>Nos bijoux</li></a>
            <a href="../../car.php"><li>Nos voitures</li></a>
            <a href="../../art.php"><li>Nos oeuvres d'art</li></a>
            <a href="../../connexion.php"><li>Se connecter</li></a>
        </ul>
    </div>
</nav>
</div>

<div class="menu-responsive">
<nav role="navigation"> 
    <div id="menuToggle"> 
        <!-- 
        A fake / hidden checkbox is used as click reciever, 
        so you can use the :checked selector on it. 
        --> 
        <input type="checkbox" /> 
 
        <!-- 
        Some spans to act as a hamburger. 
 
        They are acting like a real hamburger, 
        not that McDonalds stuff. 
        --> 
        <span></span> 
        <span></span> 
        <span></span> 
 
        <!-- 
        Too bad the menu has to be inside of the button 
        but hey, it's pure CSS magic. 
        --> 
        <ul id="menu"> 
  			<a href="../../index.php"><li>Accueil</li></a>
            <a href="../../jewel.php"><li>Nos bijoux</li></a>
            <a href="../../car.php"><li>Nos voitures</li></a>
            <a href="../../art.php"><li>Nos oeuvres d'art</li></a>
            <a href="../../connexion.php"><li>Se connecter</li></a>
        </ul> 
    </div> 
</nav>
</div>